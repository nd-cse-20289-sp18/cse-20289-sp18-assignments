#!/usr/bin/env python3

import hulk
import json

# Constants

HULK     = 'hulk.py'
HASHES   = hulk.HASHES

# Makeflow Class

class Makeflow(object):

    def __init__(self):
        self.rules = []

    def add_rule(self, command, inputs, outputs, local=False):
        rule = {
            'command': command,
            'inputs' : inputs,
            'outputs': outputs,
        }

        if local:
            rule['local_job'] = True

        self.rules.append(rule)

    def __str__(self):
        return json.dumps({
            'rules'      : self.rules,
        }, indent=4)

# Main execution

if __name__ == '__main__':
    makeflow = Makeflow()
    outputs  = []

    # Password of length 1 - 4
    for length in range(1, 5):          # TODO: Do through length 6
        output = 'p.{}'.format(length)
        makeflow.add_rule(
            './{} -l {} -s {} > {}'.format(HULK, length, HASHES, output),
            [HULK, HASHES],
            [output],
        )
        outputs.append(output)

    # Passwords of length 7, 8
    # TODO: Add rules for lengths 7 and 8 by taking advantage of prefix arguments

    # Merge all passwords
    makeflow.add_rule(
        'cat {} > passwords.txt'.format(' '.join(outputs)),
        outputs,
        ['passwords.txt'],
        True
    )

    print(makeflow)
