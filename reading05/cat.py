#!/usr/bin/env python3

import os
import sys

# Global Variables

ENDING = ''

# Usage function

def usage(status=0):
    print('''Usage: {} files...

    -E  display $ at end of each line'''.format(os.path.basename(sys.argv[0])))
    sys.exit(status)

# Parse command line options

args = sys.argv[1:]
while args and args[0].startswith('-') and len(args[0]) > 1:
    arg = args.pop(0)
    if arg == '-E':
        ENDING = '$'
    elif arg == '-h':
        usage(0)
    else:
        usage(1)

if not args:
    args.append('-')

# Main execution

for path in args:
    stream = sys.stdin if path == '-' else open(path)

    for line in stream:
        line = line.rstrip()
        print(line + ENDING)
